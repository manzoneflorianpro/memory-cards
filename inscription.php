<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="assets/css/header.css">
  <link rel="stylesheet" href="assets/css/signinsignup.css">
  <title>Inscription</title>
</head>

<body>
  <?php if (!empty($_SESSION['success'])) { ?>
    <div class="success">
      <?php echo $_SESSION['success']; ?>
    </div>
  <?php
    unset($_SESSION['success']);
  } ?>
  <?php if (!empty($_SESSION['error'])) { ?>
    <div class="error">
      <h1>Une erreur est survenue !</h1>
      <?php echo $_SESSION['error']; ?>
    </div>
  <?php
    unset($_SESSION['error']);
  } ?>
  <form action="traitement/traitementsignup.php" method="post">
    <fieldset>
      <legend>INSCRIPTION</legend>
      <input type="email" name="email" placeholder="Email" required />
      <input type="text" name="pseudo" placeholder="Pseudo" required />
      <input type="password" name="password" placeholder="Mot de passe" required />
      <input type="password" name="confirmed_password" placeholder="Confirmation du mot de passe" required />
      <input type="submit" value="S'inscrire" />
    </fieldset>
  </form>
  <p>Déja inscrit ? <a href="connexion.php">Connectez-vous</a> !</p>
</body>

</html>