<?php

require_once 'config.php';
require_once 'Classe/DAO.php';

try {
  $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
  die('Erreur : ' . $e->getMessage());
}

if (session_status() !== PHP_SESSION_ACTIVE) {
  session_start();
}

if (empty($_SESSION['pseudo'])) {
  header('location: index.php');
}


$categorie_table = new DAO($db, 'categorie');

$all_categorie = $categorie_table->find_all();

?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
  <link rel="stylesheet" href="assets/css/header.css">
  <link rel="stylesheet" href="assets/css/accueil.css" />
  <script src="assets/scripts/app.js" defer></script>
  <title>Accueil</title>
</head>

<body>
  <?php if (!empty($_SESSION['success'])) { ?>
    <div class="success">
      <?php echo $_SESSION['success']; ?>
    </div>
  <?php
    unset($_SESSION['success']);
  } ?>


  <header>
    <nav>
      <a href="accueil.php"><img src="https://see.fontimg.com/api/renderfont4/K7axe/eyJyIjoiZnMiLCJoIjo4NiwidyI6MTAwMCwiZnMiOjg2LCJmZ2MiOiIjMDAwMDAwIiwiYmdjIjoiI0ZGRkZGRiIsInQiOjF9/TWVtb3J5/hugh-is-life-personal-use-italic.png" alt="Logo Memory"></a>
      <a href="current_revision.php">Révisions en cours</a>
      <a href="decouvrir.php">Découvrir</a>
    </nav>
    <i class="fa-regular fa-circle-user" id="profil"></i>
    <div id="arrowProfil" class="arrow-up notclicked"></div>
    <div id="funcProfil" class="funcprofil notclicked">
      <a href="update_data_page.php">Modifier mes données</a>
      <a href="mycreations.php">Gérer mes créations</a>
      <a href="my_revision.php">Mes révisions</a>
      <a href="deconnexion.php">Déconnexion</a>
    </div>
  </header>
  <main>
    <h1 class="title titre--creations">Catégories <a href="CRUD/categorie/create.php">Ajouter une catégorie</a></h1>
    <!-- Sera rempli par le php -->
    <section class="categories">
      <?php
      if (empty($all_categorie)) {
        echo "Il n'y a pas de catégories";
      } else {
        for ($i = 0; $i < count($all_categorie); $i++) {
      ?>
          <article class="categorie">
            <a href="all_theme.php?id_categorie=<?php echo $all_categorie[$i]['id'] ?>">
              <?php echo $all_categorie[$i]['nom'] ?>
            </a>
          </article>
      <?php
        }
      }
      ?>
    </section>
  </main>
  <footer>
    <p>&copy; Manzone Florian | 2022</p>
  </footer>
</body>

</html>