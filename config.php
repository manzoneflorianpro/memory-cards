<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

/**
 * Nettoie les données du formulaire
 * @param Mixed $donnees Les données à nettoyer
 * @return Mixed Les données nettoyées
 */                                                                                                                 
function valid_donnees($donnees)
{
    $donnees = trim($donnees);
    $donnees = stripslashes($donnees);
    $donnees = htmlspecialchars($donnees);
    return $donnees;
}

define('DSN', 'mysql:host=localhost;dbname=memorycards;charset=utf8'); // DSN = Data Source Name
define('DB_USER', 'memorycards');
define('DB_PASS', 'memorycards123');