<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once '../../config.php';
require_once '../../Classe/DAO.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}


$categorie_functions = new DAO($db, 'categorie');

$nom = valid_donnees($_POST['name']);

if (!empty($nom)) {
    $datas = array(
        'nom' => $nom,
    );

    $categorie_functions->create($datas);
    $_SESSION['success'] = "Création de la categorie acceptée ! <br/>";
    header('location: ../../accueil.php');
} else {
    $_SESSION['error'] = "Un nom est nécessaire ! Création de la categorie refusée <br/>";
    header('location: ../../accueil.php');
}
