<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
} else {
    header('location: ../accueil.php');
}

require_once '../config.php';
require_once '../Classe/DAO.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$user_functions = new DAO($db, 'utilisateur');

$pseudo = valid_donnees($_POST['pseudo']);
$password = valid_donnees($_POST['password']);

if (!empty($pseudo)) {

    try {
        $user = $user_functions->find_by('pseudo', $pseudo);
        if (!empty($user) && password_verify($password, $user[0]['password'])) {
            session_start();

            $_SESSION['pseudo'] = $pseudo;
            $_SESSION['id'] = $user[0]['id'];

            header('location: ../accueil.php');
        } else {
            $_SESSION['error'] = "Nom d'utilisateur ou mot de passe invalide ! <br>";
            header('Location: ../connexion.php');
        }
    } catch (Exception $e) {
        die('Erreur : ' . $e->getMessage());
    }
} else {
    $_SESSION['error'] = "Nom d'utilisateur ou mot de passe invalide ! <br>";
    header('Location: ../index.php');
}
