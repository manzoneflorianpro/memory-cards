<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once '../../config.php';
require_once '../../Classe/DAO.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}


$theme_functions = new DAO($db, 'theme');
$carte_functions = new DAO($db, 'carte');

$recto = valid_donnees($_POST['recto']);
$verso = valid_donnees($_POST['verso']);
$id_theme = $_GET['id_theme'];
$current_date = date('Y-m-d');
$uploads_dir = '../../assets/uploads/';

// Tableau des extensions que l'on accepte
$extensions = ['jpg', 'png', 'jpeg'];

if (!empty($_FILES['img_Recto'])) {
    $tmpNameRecto = $_FILES['img_Recto']['tmp_name'];
    $nameRecto = $_FILES['img_Recto']['name'];
    $sizeRecto = $_FILES['img_Recto']['size'];
    $errorRecto = $_FILES['img_Recto']['error'];

    // On récupère l'extension de l'image
    $tabExtensionRecto = explode('.', $nameRecto);
    $extensionRecto = strtolower(end($tabExtensionRecto));

    // On vérifie l'extension de l'image upload pour le recto
    if (in_array($extensionRecto, $extensions) && $errorRecto == 0) {
        $nameRecto_unique = strtolower($tabExtensionRecto[0]) . rand(0, 10000) . $extensionRecto;
        move_uploaded_file($tmpNameRecto, $uploads_dir . $nameRecto_unique);
    }
}

if (!empty($_FILES['img_Verso'])) {
    $tmpNameVerso = $_FILES['img_Verso']['tmp_name'];
    $nameVerso = $_FILES['img_Verso']['name'];
    $sizeVerso = $_FILES['img_Verso']['size'];
    $errorVerso = $_FILES['img_Verso']['error'];

    // On récupère l'extension de l'image
    $tabExtensionVerso = explode('.', $nameVerso);
    $extensionVerso = strtolower(end($tabExtensionVerso));

    // On vérifie l'extension de l'image upload pour le verso
    if (in_array($extensionVerso, $extensions) && $errorVerso == 0) {
        $nameVerso_unique = strtolower($tabExtensionVerso[0]) . rand(0, 10000) . $extensionVerso;
        move_uploaded_file($tmpNameVerso, $uploads_dir . $nameVerso_unique);
    }
}

if (!empty($recto) || !empty($_FILES['img_Recto']) && !empty($verso) || !empty($_FILES['img_Verso'])) {

    if (empty($recto)) {
        $recto = null;
    }

    if (empty($verso)) {
        $verso = null;
    }

    if (empty($_FILES['img_Recto'])) {
        $nameRecto = null;
    }

    if (empty($_FILES['img_Verso'])) {
        $nameVerso = null;
    }

    $datas = array(
        'recto' => $recto,
        'verso' => $verso,
        'img_recto' => $nameRecto_unique,
        'img_verso' => $nameVerso_unique,
        'date_creation' => $current_date,
        'date_modification' => $current_date,
        'id_theme' => $id_theme
    );
    $carte_functions->create($datas);
    $_SESSION['success'] = "Création réussie <br/>";
    header('location: ../../theme.php?id_theme=' . $id_theme);
} else {
    $_SESSION['error'] = "Un champ requis n'a pas été rempli ! Carte refusée <br/>";
    header('location: ../../theme.php?id_theme=' . $id_theme);
}
