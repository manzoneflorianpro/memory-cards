<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once '../../config.php';
require_once '../../Classe/DAO.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}


$user_functions = new DAO($db, 'utilisateur');
$theme_functions = new DAO($db, 'theme');

$id_categorie = valid_donnees($_POST['categorie']);
$nom = valid_donnees($_POST['nom']);
$description = valid_donnees($_POST['description']);
$id_theme = $_GET['id_theme'];
$date = date('Y-m-d');

if ($_POST['public'] == "on") {
    $public = true;
} else {
    $public = 0;
}

if (!empty($nom) && !empty($id_categorie)) {
    $datas = array(
        'nom' => $nom,
        'description' => $description,
        'public' => $public,
        'id_categorie' => $id_categorie,
        'id_user' => $_SESSION['id'],
        'date_creation' => $date
    );
    $theme_functions->create($datas);
    $_SESSION['success'] = "Création du thème acceptée ! <br/>";
    header('location: ../../mycreations.php');
} else {
    $_SESSION['error'] = "Un nom et une catégorie sont nécessaires ! Création du thème refusée <br/>";
    header('location: ../../mycreations.php');
}
