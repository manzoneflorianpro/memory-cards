<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once '../../config.php';
require_once '../../Classe/DAO.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}


$user_functions = new DAO($db, 'utilisateur');
$theme_functions = new DAO($db, 'theme');

$nom = valid_donnees($_POST['nom']);
$description = valid_donnees($_POST['description']);
$id_theme = $_GET['id_theme'];

if (!empty($_POST['public'])) {
    $public = 1;
} else {
    $public = 0;
}


if (!empty($nom) && !empty($id_theme)) {
    if ($public == true || $public == false) {
        $datas = array(
            'nom' => $nom,
            'description' => $description,
            'public' => $public,
        );

        $theme_functions->update($datas, 'id', $id_theme);
        $_SESSION['success'] = "Modification du thème acceptée ! <br/>";
        header('location: ../../theme.php?id_theme=' . $id_theme);
    } else {
        $_SESSION['error'] = "Modification du thème refusée";
    }
} else {
    $_SESSION['error'] = "Un nom est nécessaire ! Modification du thème refusée <br/>";
    header('location: ../../CRUD/theme/update.php?id_theme=' . $id_theme);
}
