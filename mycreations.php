<?php

require_once 'config.php';
require_once 'Classe/DAO.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

if (empty($_SESSION['pseudo'])) {
    header('location: index.php');
}

$categories_functions = new DAO($db, 'categorie');
$themes_functions = new DAO($db, 'theme');
$all_cards_functions = new DAO($db, 'carte');

$all_categories = $categories_functions->find_all();
$all_themes = $themes_functions->find_all();
$all_cards = $all_cards_functions->find_all();

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/header.css">
    <link rel="stylesheet" href="assets/css/theme.css">
    <link rel="stylesheet" href="assets/css/decouvrir.css">
    <script src="assets/scripts/app.js" defer></script>
    <title>Mes créations</title>
</head>

<body>
    <?php if (!empty($_SESSION['success'])) { ?>
        <div class="success">
            <?php echo $_SESSION['success']; ?>
        </div>
    <?php
        unset($_SESSION['success']);
    } ?>
    <?php if (!empty($_SESSION['error'])) { ?>
        <div class="error">
            <h1>Une erreur est survenue !</h1>
            <?php echo $_SESSION['error']; ?>
        </div>
    <?php
        unset($_SESSION['error']);
    } ?>
    <header>
        <nav>
            <a href="accueil.php"><img src="https://see.fontimg.com/api/renderfont4/K7axe/eyJyIjoiZnMiLCJoIjo4NiwidyI6MTAwMCwiZnMiOjg2LCJmZ2MiOiIjMDAwMDAwIiwiYmdjIjoiI0ZGRkZGRiIsInQiOjF9/TWVtb3J5/hugh-is-life-personal-use-italic.png" alt="Logo Memory"></a>
            <a href="current_revision.php">Révisions en cours</a>
            <a href="decouvrir.php">Découvrir</a>
        </nav>
        <i class="fa-regular fa-circle-user" id="profil"></i>
        <div id="arrowProfil" class="arrow-up notclicked"></div>
        <div id="funcProfil" class="funcprofil notclicked">
            <a href="update_data_page.php">Modifier mes données</a>
            <a href="mycreations.php">Gérer mes créations</a>
            <a href="my_revision.php">Mes révisions</a>
            <a href="deconnexion.php">Déconnexion</a>
        </div>
    </header>
    <main>
        <!-- Rempli par BDD et PHP -->
        <!-- Thèmes -->
        <h1 class="titre--creations">Mes thèmes <a href="CRUD/theme/create.php">Ajouter un thème</a></h1>
        <section class="theme">
            <?php for ($i = 0; $i < count($all_themes); $i++) {
                if ($all_themes[$i]['id_user'] == $_SESSION['id']) {
            ?>
                    <article>
                        <a href="theme.php?id_theme=<?php echo $all_themes[$i]['id'] ?>"><?php echo $all_themes[$i]['nom'] ?></a>
                    </article>
            <?php
                }
            } ?>
        </section>

        <h1 class="titre--creations">Mes cartes</h1>
        <section class="carte">
            <?php for ($i = 0; $i < count($all_cards); $i++) {

                $theme = $themes_functions->find_by('id', $all_cards[$i]['id_theme']);

                if ($theme[0]['id_user'] == $_SESSION['id']) {
            ?>
                    <article>
                        <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                            <div class="flipper">
                                <div class="front">
                                    <!-- front content -->
                                    <?php
                                    // On vérifie si il y a une image au recto
                                    if ($all_cards[$i]['img_recto'] !== NULL) {
                                    ?>
                                        <img src="assets/uploads/<?php echo $all_cards[$i]['img_recto'] ?>" alt="image choisie par l'utilisateur">
                                    <?php
                                    }
                                    // On vérifie si il y a un texte au recto
                                    if ($all_cards[$i]['recto']) {
                                    ?>
                                        <p><?php echo $all_cards[$i]['recto']; ?></p>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="back">
                                    <!-- back content -->
                                    <?php
                                    // On vérifie si il y a une image au recto
                                    if ($all_cards[$i]['img_verso'] !== NULL) {
                                    ?>
                                        <img src="assets/uploads/<?php $all_cards[$i]['img_verso'] ?>" alt="image choisie par l'utilisateur">
                                    <?php
                                    }
                                    // On vérifie si il y a un texte au recto
                                    if ($all_cards[$i]['verso']) {
                                    ?>
                                        <p><?php echo $all_cards[$i]['verso']; ?></p>
                                        <!-- <p class="categorie--cards"><?php  ?></p> -->
                                    <?php
                                    }
                                    ?>
                                    <?php if ($theme[0]['id_user'] == $_SESSION['id']) { ?>
                                        <div class="back-CRUD">
                                            <a href="CRUD/card/update.php?id_carte=<?php echo $all_cards[$i]['id'] ?>&id_theme=<?php echo $all_cards[$i]['id_theme'] ?>"><i class="fa-sharp fa-solid fa-pen"></i></a>
                                            <a href="CRUD/card/delete.php?id_carte=<?php echo $all_cards[$i]['id'] ?>&id_theme=<?php echo $all_cards[$i]['id_theme'] ?>"><i class="fa-solid fa-trash"></i></a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </article>
            <?php
                }
            } ?>
        </section>

    </main>
</body>

</html>