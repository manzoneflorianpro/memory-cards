const profil = document.getElementById("profil");
const funcProfil = document.getElementById("funcProfil");
const main = document.querySelector("main");
const arrow = document.getElementById("arrowProfil");

profil.addEventListener("click", () => {
  if (funcProfil.classList.contains("notclicked")) {
    funcProfil.classList.remove("notclicked");
    arrow.classList.remove("notclicked");
  } else {
    funcProfil.classList.add("notclicked");
    arrow.classList.add("notclicked");
  }
});

main.addEventListener("click", () => {
  funcProfil.classList.add("notclicked");
  arrow.classList.add("notclicked");
});
