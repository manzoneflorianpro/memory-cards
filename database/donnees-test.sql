USE memorycards;

INSERT INTO
    utilisateur(email, pseudo, password)
VALUES
    (
        "certification@gmail.com",
        "certif",
        "$2y$10$ftFTBjeFRjhrIlvI7D0bueiCWn2mH9/xWtRt3bpHu9w8BsrkbXzbK" -- Florian
    );


INSERT INTO
    categorie(nom)
VALUES
    ('Langues');

INSERT INTO
    theme(
        nom,
        description,
        public,
        date_creation,
        id_user,
        id_categorie
    )
VALUES
    (
        "Anglais",
        "Anglais débutant",
        true,
        "2022-10-26",
        1,
        1
    );

INSERT INTO
    carte(
        recto,
        verso,
        date_creation,
        id_theme,
        date_modification
    )
VALUES
    (
        "Yes",
        "Oui",
        "2022-10-26",
        1,
        "2022-10-26"
    ),
    (
        "No",
        "Non",
        "2022-10-26",
        1,
        "2022-10-26"
    );
