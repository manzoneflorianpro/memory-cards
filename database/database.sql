DROP DATABASE memorycards;
CREATE DATABASE IF NOT EXISTS memorycards;

USE memorycards;

CREATE TABLE IF NOT EXISTS utilisateur (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(100) NOT NULL UNIQUE,
    pseudo VARCHAR(30) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS categorie (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS theme (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nom VARCHAR(50) NOT NULL,
    description TEXT NULL,
    public BOOLEAN NOT NULL,
    date_creation DATE NOT NULL,
    id_user INT,
    id_categorie INT,
    FOREIGN KEY (id_user) REFERENCES utilisateur(id) ON DELETE CASCADE,
    FOREIGN KEY (id_categorie) REFERENCES categorie(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS revision (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nb_niveau INT NOT NULL,
    nb_cartes INT NOT NULL DEFAULT 1,
    started_at DATE NOT NULL,
    id_user INT,
    id_theme INT,
    FOREIGN KEY (id_user) REFERENCES utilisateur(id),
    FOREIGN KEY (id_theme) REFERENCES theme(id)
);

CREATE TABLE IF NOT EXISTS carte (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    recto VARCHAR(255) NULL,
    verso VARCHAR(255) NULL,
    img_recto VARCHAR(255) NULL,
    img_verso VARCHAR(255) NULL,
    date_creation DATE NOT NULL,
    date_modification DATE NOT NULL,
    id_theme INT,
    FOREIGN KEY (id_theme) REFERENCES theme(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS revoit (
    id_revision INT,
    id_carte INT,
    derniere_vu DATE NOT NULL,
    niveau INT NOT NULL,
    PRIMARY KEY (id_revision, id_carte),
    FOREIGN KEY (id_revision) REFERENCES revision(id),
    FOREIGN KEY (id_carte) REFERENCES carte(id)
);

CREATE USER IF NOT EXISTS 'memorycards' @'localhost' IDENTIFIED by 'memorycards123';

GRANT ALL ON memorycards.* TO 'memorycards' @'localhost';

FLUSH PRIVILEGES;