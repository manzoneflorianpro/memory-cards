<?php

require_once 'config.php';
require_once 'Classe/DAO.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

if (empty($_SESSION['pseudo'])) {
    header('location: index.php');
}

$themes_functions = new DAO($db, 'theme');
$cartes_functions = new DAO($db, 'carte');

if (!empty($_GET['id_theme'])) {
    $id_theme = $_GET['id_theme'];
    $selected_theme = $themes_functions->find_by('id', $id_theme);
    $cartes = $cartes_functions->find_by('id_theme', $id_theme);
}


?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/header.css">
    <link rel="stylesheet" href="assets/css/theme.css">
    <script src="assets/scripts/app.js" defer></script>
    <title>Thèmes</title>
</head>

<body>
    <?php if (!empty($_SESSION['success'])) { ?>
        <div class="success">
            <?php echo $_SESSION['success']; ?>
        </div>
    <?php
        unset($_SESSION['success']);
    } ?>
    <?php if (!empty($_SESSION['error'])) { ?>
        <div class="error">
            <h1>Une erreur est survenue !</h1>
            <?php echo $_SESSION['error']; ?>
        </div>
    <?php
        unset($_SESSION['error']);
    } ?>
    <header>
        <nav>
            <a href="accueil.php"><img src="https://see.fontimg.com/api/renderfont4/K7axe/eyJyIjoiZnMiLCJoIjo4NiwidyI6MTAwMCwiZnMiOjg2LCJmZ2MiOiIjMDAwMDAwIiwiYmdjIjoiI0ZGRkZGRiIsInQiOjF9/TWVtb3J5/hugh-is-life-personal-use-italic.png" alt="Logo Memory"></a>
            <a href="current_revision.php">Révisions en cours</a>
            <a href="decouvrir.php">Découvrir</a>
        </nav>
        <i class="fa-regular fa-circle-user" id="profil"></i>
        <div id="arrowProfil" class="arrow-up notclicked"></div>
        <div id="funcProfil" class="funcprofil notclicked">
            <a href="update_data_page.php">Modifier mes données</a>
            <a href="mycreations.php">Gérer mes créations</a>
            <a href="my_revision.php">Mes révisions</a>
            <a href="deconnexion.php">Déconnexion</a>
        </div>
    </header>
    <main>
        <section class="theme">

            <?php if ($selected_theme[0]['id_user'] == $_SESSION['id']) { ?>
                <article class="crud">
                    <a href="CRUD/card/create.php?id_theme=<?php echo $id_theme ?>" class="add" title="Ajouter une carte"><i class="fa-regular fa-plus"></i></a>
                    <a href="CRUD/theme/update.php?id_theme=<?php echo $id_theme ?>" class="update" title="Mettre à jour le thème"><i class="fa-solid fa-pen"></i></a>
                    <a href="CRUD/theme/delete.php?id_theme=<?php echo $id_theme ?>" class="delete" title="Effacer le thème"><i class="fa-solid fa-trash"></i></a>
                </article>
            <?php } ?>
            
            <article>
                <h1><?php echo $selected_theme[0]['nom'] ?></h1>
                <p><?php echo $selected_theme[0]['description'] ?></p>
            </article>
        </section>
        <section class="cartes">
            <!-- Rempli par PHP -->
            <?php
            for ($i = 0; $i < count($cartes); $i++) {
            ?>
                <div class="flip-container" ontouchstart="this.classList.toggle('hover');">
                    <div class="flipper">
                        <div class="front">
                            <!-- front content -->
                            <?php
                            // On vérifie si il y a une image au recto
                            if ($cartes[$i]['img_recto'] !== NULL) {
                            ?>
                                <img src="assets/uploads/<?php echo $cartes[$i]['img_recto'] ?>" alt="image choisie par l'utilisateur">
                            <?php
                            }
                            // On vérifie si il y a un texte au recto
                            if ($cartes[$i]['recto']) {
                            ?>
                                <p><?php echo $cartes[$i]['recto']; ?></p>
                            <?php
                            }
                            ?>
                        </div>
                        <div class="back">
                            <!-- back content -->
                            <?php
                            // On vérifie si il y a une image au recto
                            if ($cartes[$i]['img_verso'] !== NULL) {
                            ?>
                                <img src="assets/uploads/<?php $cartes[$i]['img_verso'] ?>" alt="image choisie par l'utilisateur">
                            <?php
                            }
                            // On vérifie si il y a un texte au recto
                            if ($cartes[$i]['verso']) {
                            ?>
                                <p><?php echo $cartes[$i]['verso']; ?></p>
                            <?php
                            }
                            ?>
                            <?php if ($selected_theme[0]['id_user'] == $_SESSION['id']) { ?>
                                <div class="back-CRUD">
                                    <a href="CRUD/card/update.php?id_carte=<?php echo $cartes[$i]['id'] ?>&id_theme=<?php echo $id_theme ?>"><i class="fa-sharp fa-solid fa-pen"></i></a>
                                    <a href="CRUD/card/delete.php?id_carte=<?php echo $cartes[$i]['id'] ?>&id_theme=<?php echo $id_theme ?>"><i class="fa-solid fa-trash"></i></a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            <?php
            }
            ?>
        </section>
        <section class="reviser">
            <a href="#">
                <!-- Pour qu'il soit bien placé il faut que la page soit rempli -->
                <h1>Lancer la révision</h1>
            </a>

        </section>
    </main>
</body>

</html>