<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

if (!isset($_SESSION['pseudo'])) {
    header('location: ../index.php');
}

require_once '../../config.php';
require_once '../../Classe/DAO.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$theme_functions = new DAO($db, 'theme');

if (!empty($_GET['id_theme'])) {
    $id_theme = $_GET['id_theme'];

    $theme = $theme_functions->find_by('id', $id_theme);

    if ($theme[0]['id_user'] == $_SESSION['id']) {
        $theme_functions->delete('id', $id_theme);
        header('location: ../../accueil.php');
    } else {
        header('location: ../../accueil.php');
    }
} else {
        header('location: ../../accueil.php');
}
