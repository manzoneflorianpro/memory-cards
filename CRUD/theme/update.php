<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

if (empty($_SESSION['pseudo'])) {
    header('location: ../../index.php');
}

require_once '../../config.php';
require_once '../../Classe/DAO.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$user_functions = new DAO($db, 'utilisateur');
$categorie_functions = new DAO($db, 'categorie');
$theme_functions = new DAO($db, 'theme');

$categories = $categorie_functions->find_all();

if (!empty($_GET['id_theme'])) {

    $id_theme = $_GET['id_theme'];

    $theme_to_update = $theme_functions->find_by('id', $id_theme);


    if (gettype($theme_to_update) == 'array' && $theme_to_update[0]['id_user'] == $_SESSION['id']) {
        $name = $theme_to_update[0]['nom'];
        $description = $theme_to_update[0]['description'];
    } else {
        header('location: ../../theme.php?id_theme=' . $id_theme);
    }
} else {
    if (session_status() !== PHP_SESSION_ACTIVE) {
        header('location: ../../index.php');
    } else {
        header('location: ../../theme.php?id_theme=' . $id_theme);
    }
}
?>


<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
    <link rel="stylesheet" href="../../assets/css/header.css">
    <link rel="stylesheet" href="../../assets/css/signinsignup.css">
    <script src="../../assets/scripts/app.js" defer></script>
    <title>Update theme</title>
</head>

<body>
    <header>
        <nav>
            <a href="../../accueil.php"><img src="https://see.fontimg.com/api/renderfont4/K7axe/eyJyIjoiZnMiLCJoIjo4NiwidyI6MTAwMCwiZnMiOjg2LCJmZ2MiOiIjMDAwMDAwIiwiYmdjIjoiI0ZGRkZGRiIsInQiOjF9/TWVtb3J5/hugh-is-life-personal-use-italic.png" alt="Logo Memory"></a>
            <a href="../../current_revision.php">Révisions en cours</a>
            <a href="../../decouvrir.php">Découvrir</a>
        </nav>
        <i class="fa-regular fa-circle-user" id="profil"></i>
        <div id="arrowProfil" class="arrow-up notclicked"></div>
        <div id="funcProfil" class="funcprofil notclicked">
            <a href="../../update_data_page.php">Modifier mes données</a>
            <a href="../../mycreations.php">Gérer mes créations</a>
            <a href="../../my_revision.php">Mes révisions</a>
            <a href="../../deconnexion.php">Déconnexion</a>
        </div>
    </header>
    <main>

        <form action="../../traitement/theme/traitement_update_theme.php?id_theme=<?php echo $id_theme ?>" method="POST">
            <fieldset>
                <legend>UPDATE</legend>
                <!-- <select name="categorie">
                    <option value="categorie">-- Categorie --</option> -->
                    <?php
                    /*for ($i = 0; $i < count($categories); $i++) {
                    ?>
                        <option value="<?php echo $categories[$i]['id'] ?>" <?php if ($theme_to_update[0]['id_categorie'] == $categories[$i]['id']) {
                                                                                echo "selected";
                                                                                $categorie = $categories[$i]['nom'];
                                                                            } ?>><?php echo $categories[$i]['nom'] ?></option>
                    <?php
                    }*/
                    ?>
                <!-- </select>
                <label for="createCategorie">Créer une catégorie si inexistante :</label>
                <input type="text" name="createCategorie" placeholder="Sport" value="<?php //echo $categorie ?>"> -->
                <label for="nom">Nom :</label>
                <input type="text" name="nom" placeholder="Nom" value="<?php echo $name ?>" required>
                <label for="description">Description :</label>
                <textarea name="description" cols="30" rows="5"><?php echo $description ?></textarea>
                <div class="checkbox">
                    <input type="checkbox" name="public" <?php if ($theme_to_update[0]['public'] == true) echo "checked" ?>>
                    <label for="public">Public</label>
                </div>
                <div>
                    <input type="submit" value="Modifier">
                    <a href="../../theme.php?id_theme=<?php echo $id_theme ?>" id="annulation">Annuler</a>
                </div>
            </fieldset>
        </form>
    </main>
</body>

</html>