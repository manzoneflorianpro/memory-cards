<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

if (empty($_SESSION['pseudo'])) {
    header('location: ../../index.php');
}

require_once '../../config.php';
require_once '../../Classe/DAO.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

if (!empty($_GET['id_theme'])) {

    $id_theme = $_GET['id_theme'];
}
?>


<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
    <link rel="stylesheet" href="../../assets/css/header.css">
    <link rel="stylesheet" href="../../assets/css/signinsignup.css">
    <script src="../../assets/scripts/app.js" defer></script>
    <title>Create card</title>
</head>

<body>
    <header>
        <nav>
            <a href="../../accueil.php"><img src="https://see.fontimg.com/api/renderfont4/K7axe/eyJyIjoiZnMiLCJoIjo4NiwidyI6MTAwMCwiZnMiOjg2LCJmZ2MiOiIjMDAwMDAwIiwiYmdjIjoiI0ZGRkZGRiIsInQiOjF9/TWVtb3J5/hugh-is-life-personal-use-italic.png" alt="Logo Memory"></a>
            <a href="../../current_revision.php">Révisions en cours</a>
            <a href="../../decouvrir.php">Découvrir</a>
        </nav>
        <i class="fa-regular fa-circle-user" id="profil"></i>
        <div id="arrowProfil" class="arrow-up notclicked"></div>
        <div id="funcProfil" class="funcprofil notclicked">
            <a href="../../update_data_page.php">Modifier mes données</a>
            <a href="../../mycreations.php">Gérer mes créations</a>
            <a href="../../my_revision.php">Mes révisions</a>
            <a href="../../deconnexion.php">Déconnexion</a>
        </div>
    </header>
    <main>

        <form action="../../traitement/card/traitement_create_card.php?id_theme=<?php echo $id_theme ?>" method="POST">
            <fieldset>
                <legend>CREATE</legend>
                <label for="recto">Recto :</label>
                <input type="text" name="recto" placeholder="Recto">
                <label for="verso">Verso :</label>
                <input type="text" name="verso" placeholder="Verso">
                <label for="img_Recto">Image Recto :</label>
                <input type="file" name="img_Recto">
                <label for="img_Verso">Image Verso :</label>
                <input type="file" name="img_Verso">
                <div>
                    <input type="submit" value="Ajouter">
                    <a href="../../theme.php?id_theme=<?php echo $id_theme ?>" id="annulation">Annuler</a>
                </div>
            </fieldset>
        </form>
    </main>
</body>

</html>