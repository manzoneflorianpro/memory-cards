<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

if (!isset($_SESSION['pseudo'])) {
    header('location: ../index.php');
}

require_once '../../config.php';
require_once '../../Classe/DAO.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$theme_functions = new DAO($db, 'theme');
$card_functions = new DAO($db, 'carte');

if (!empty($_GET['id_carte']) && !empty($_GET['id_theme'])) {
    $id_carte = $_GET['id_carte'];
    $id_theme = $_GET['id_theme'];

    $cards_theme = $theme_functions->find_by('id', $id_theme);

    if ($cards_theme[0]['id_user'] == $_SESSION['id']) {
        $card_functions->delete('id', $id_carte);
        header('location: ../../theme.php?id_theme= ' . $id_theme);
    } else {
        header('location: ../../theme.php?id_theme= ' . $id_theme);
    }
} else {
        header('location: ../../theme.php?id_theme= ' . $id_theme);
}
