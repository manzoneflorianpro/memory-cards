<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
} else {
    header("Location: ../page2.php");
}

require_once '../config.php';
require_once '../Classe/DAO.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$user_functions = new DAO($db, 'utilisateur');

$email = valid_donnees($_POST['email']);
$pseudo = valid_donnees($_POST['pseudo']);
$current_password = valid_donnees($_POST['current_password']);
$password = valid_donnees($_POST['password']);
$password_confirmed = valid_donnees($_POST['confirmed_password']);

filter_input(INPUT_POST, $email, FILTER_SANITIZE_EMAIL);

if (!empty($_SESSION['id'])) {

    $user = $user_functions->find_by('id', $_SESSION['id']);
    if (!empty($user) && password_verify($current_password, $user[0]['password'])) {

        if (!empty($pseudo) && !empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL) && $password === $password_confirmed) {

            $hashed_password = password_hash($password, PASSWORD_DEFAULT);
            $datas = array('email' => $email, 'pseudo' => $pseudo, 'password' => $hashed_password);
            try {
                $user_functions->update($datas, "id", $_SESSION['id']);
                header("Location: ../accueil.php");
            } catch (Exception $e) {
                die('Erreur : ' . $e->getMessage());
            }
        } else {
            $_SESSION['error'] = "Un champ n'a pas été rempli ou mots de passe différents ! Données non modifées ! <br/>";
        }
    } // Faire un else pour indiquer l'erreur en session si il y en a une
} else {
    header('Location: ../index.php');
}
