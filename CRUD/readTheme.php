<?php

require_once '../config.php';
require_once '../Classe/DAO.php';

try {
  $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
  die('Erreur : ' . $e->getMessage());
}

if (session_status() !== PHP_SESSION_ACTIVE) {
  session_start();
}

if (empty($_SESSION['pseudo'])) {
  header('location: index.php');
}
