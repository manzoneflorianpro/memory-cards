<?php

require_once 'config.php';
require_once 'Classe/DAO.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

if (empty($_SESSION['pseudo'])) {
    header('location: index.php');
}

$categorie_functions = new DAO($db, 'categorie');
$theme_functions = new DAO($db, 'theme');

$categories = $categorie_functions->find_all();

$themes_correspondants = [];

if (!empty($_POST['categorie_selected']) && $_POST['categorie_selected'] !== "categorie") {
    $id_categorie = $_POST['categorie_selected'];
    $categorie_selected = $categorie_functions->find_by('id', $id_categorie);
    $themes_correspondants = $theme_functions->find_by('id_categorie', $id_categorie);
}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/header.css">
    <link rel="stylesheet" href="assets/css/decouvrir.css" />
    <script src="assets/scripts/app.js" defer></script>
    <title>Découvrir</title>
</head>

<body>
    <?php if (!empty($_SESSION['success'])) { ?>
        <div class="success">
            <?php echo $_SESSION['success']; ?>
        </div>
    <?php
        unset($_SESSION['success']);
    } ?>
    <?php if (!empty($_SESSION['error'])) { ?>
        <div class="error">
            <h1>Une erreur est survenue !</h1>
            <?php echo $_SESSION['error']; ?>
        </div>
    <?php
        unset($_SESSION['error']);
    } ?>
    <header>
        <nav>
            <a href="accueil.php"><img src="https://see.fontimg.com/api/renderfont4/K7axe/eyJyIjoiZnMiLCJoIjo4NiwidyI6MTAwMCwiZnMiOjg2LCJmZ2MiOiIjMDAwMDAwIiwiYmdjIjoiI0ZGRkZGRiIsInQiOjF9/TWVtb3J5/hugh-is-life-personal-use-italic.png" alt="Logo Memory"></a>
            <a href="current_revision.php">Révisions en cours</a>
            <a href="decouvrir.php">Découvrir</a>
        </nav>
        <i class="fa-regular fa-circle-user" id="profil"></i>
        <div id="arrowProfil" class="arrow-up notclicked"></div>
        <div id="funcProfil" class="funcprofil notclicked">
            <a href="update_data_page.php">Modifier mes données</a>
            <a href="mycreations.php">Gérer mes créations</a>
            <a href="my_revision.php">Mes révisions</a>
            <a href="deconnexion.php">Déconnexion</a>
        </div>
    </header>
    <main>
        <form action="decouvrir.php" method="post">
            <select name="categorie_selected" id="chooser">
                <option value="categorie">-- Categorie --</option>
                <?php
                for ($i = 0; $i < count($categories); $i++) {
                ?>
                    <option value="<?php echo $categories[$i]['id'] ?>"><?php echo $categories[$i]['nom'] ?></option>
                <?php
                }
                ?>
            </select>
            <input type="submit" value="Rechercher">
        </form>
        <h1 class="titre"><?php !empty($_POST['categorie_selected']) && $_POST['categorie_selected'] !== "categorie" ? print_r($categorie_selected[0]['nom']) : ""; ?></h1>
        <section class="categorie">
            <?php
            if (!empty($themes_correspondants)) {
                for ($i = 0; $i < count($themes_correspondants); $i++) {
                    if (!$themes_correspondants[$i]['public'] && $themes_correspondants[$i]['id_user'] !== $_SESSION['id']) {
                        unset($themes_correspondants[$i]);
                    }
                }
                for ($i = 0; $i < count($themes_correspondants); $i++) {
            ?>
                    <article>
                        <a href="theme.php?id_theme=<?php echo $themes_correspondants[$i]['id'] ?>"><?php echo $themes_correspondants[$i]['nom'] ?></a>
                    </article>
            <?php
                }
            } else if (empty($_POST['categorie_selected']) || $_POST['categorie_selected'] == "categorie") {
                echo "Choisissez une catégorie";
            } else {
                echo "Il n'y a pas encore de thème dans cette catégorie...";
            }
            ?>

        </section>
    </main>
</body>

</html>