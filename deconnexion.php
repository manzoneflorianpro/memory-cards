<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

session_unset();
setcookie(session_name(), '', strtotime('- 1 day'));
unset($_COOKIE[session_name()]);
session_destroy();
header('Location: index.php');
