<?php

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

if (empty($_SESSION['pseudo'])) {
    header('location: index.php');
}

require_once 'Classe/DAO.php';
require_once 'config.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$user_functions = new DAO($db, 'utilisateur');

if (!empty($_SESSION['id'])) {
    $user = $user_functions->find_by('id', $_SESSION['id']);
}


?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/header.css">
    <link rel="stylesheet" href="assets/css/signinsignup.css">
    <script src="assets/scripts/app.js" defer></script>

    <title>Modifier mes données</title>
</head>

<body>
    <?php if (!empty($_SESSION['success'])) { ?>
        <div class="success">
            <?php echo $_SESSION['success']; ?>
        </div>
    <?php
        unset($_SESSION['success']);
    } ?>
    <?php if (!empty($_SESSION['error'])) { ?>
        <div class="error">
            <h1>Une erreur est survenue !</h1>
            <?php echo $_SESSION['error']; ?>
        </div>
    <?php
        unset($_SESSION['error']);
    } ?>
    <header>
        <nav>
            <a href="accueil.php"><img src="https://see.fontimg.com/api/renderfont4/K7axe/eyJyIjoiZnMiLCJoIjo4NiwidyI6MTAwMCwiZnMiOjg2LCJmZ2MiOiIjMDAwMDAwIiwiYmdjIjoiI0ZGRkZGRiIsInQiOjF9/TWVtb3J5/hugh-is-life-personal-use-italic.png" alt="Logo Memory"></a>
            <a href="current_revision.php">Révisions en cours</a>
            <a href="decouvrir.php">Découvrir</a>
        </nav>
        <i class="fa-regular fa-circle-user" id="profil"></i>
        <div id="arrowProfil" class="arrow-up notclicked"></div>
        <div id="funcProfil" class="funcprofil notclicked">
            <a href="update_data_page.php">Modifier mes données</a>
            <a href="mycreations.php">Gérer mes créations</a>
            <a href="my_revision.php">Mes révisions</a>
            <a href="deconnexion.php">Déconnexion</a>
        </div>
    </header>
    <main>
        <form action="CRUD/updatedata.php" method="post">
            <fieldset>
                <!-- Remplissage des inputs par PHP -->
                <legend>Mise à jour</legend>
                <input type="email" name="email" required value="<?php echo $user[0]['email'] ?>" />
                <input type="text" name="pseudo" required value="<?php echo $user[0]['pseudo'] ?>" />
                <input type="password" name="current_password" placeholder="Mot de passe actuel" required />
                <input type="password" name="password" placeholder="Mot de passe" required />
                <input type="password" name="confirmed_password" placeholder="Confirmation du mot de passe" required />
                <input type="submit" value="Modifier" />
            </fieldset>
        </form>
    </main>
</body>

</html>