<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/header.css">
    <link rel="stylesheet" href="assets/css/signinsignup.css">
    <title>Connexion</title>
</head>

<body class="connexion">
    <?php if (!empty($_SESSION['success'])) { ?>
        <div class="success">
            <?php echo $_SESSION['success']; ?>
        </div>
    <?php
        unset($_SESSION['success']);
    } ?>
    <?php if (!empty($_SESSION['error'])) { ?>
        <div class="error">
            <h1>Une erreur est survenue !</h1>
            <?php echo $_SESSION['error']; ?>
        </div>
    <?php
        unset($_SESSION['error']);
    } ?>
    <form action="traitement/traitementsignin.php" method="post">
        <fieldset>
            <legend>CONNEXION</legend>
            <input type="text" name="pseudo" placeholder="Pseudo" required>
            <input type="password" name="password" placeholder="Mot de passe" required>
            <input type="submit" value="Se connecter">
        </fieldset>
    </form>
    <p>Pas encore inscrit ? <a href="inscription.php">Inscrivez-vous</a> !</p>
</body>

</html>