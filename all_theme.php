<?php

require_once 'config.php';

try {
    $db = new PDO(DSN, DB_USER, DB_PASS);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

if (empty($_SESSION['id'])) {
    header('location: index.php');
}

require_once 'Classe/DAO.php';

$categorie_table = new DAO($db, 'categorie');
$theme_table = new DAO($db, 'theme');

$categorie = $categorie_table->find_by('id', $_GET['id_categorie']);
$themes = $theme_table->find_all();

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
    <link rel="stylesheet" href="assets/css/header.css">
    <link rel="stylesheet" href="assets/css/decouvrir.css">
    <script src="assets/scripts/app.js" defer></script>
    <title><?php print_r($categorie[0]['nom']) ?></title>
</head>

<body>
    <?php if (!empty($_SESSION['success'])) { ?>
        <div class="success">
            <?php echo $_SESSION['success']; ?>
        </div>
    <?php
        unset($_SESSION['success']);
    } ?>
    <?php if (!empty($_SESSION['error'])) { ?>
        <div class="error">
            <h1>Une erreur est survenue !</h1>
            <?php echo $_SESSION['error']; ?>
        </div>
    <?php
        unset($_SESSION['error']);
    } ?>
    <header>
        <nav>
            <a href="accueil.php"><img src="https://see.fontimg.com/api/renderfont4/K7axe/eyJyIjoiZnMiLCJoIjo4NiwidyI6MTAwMCwiZnMiOjg2LCJmZ2MiOiIjMDAwMDAwIiwiYmdjIjoiI0ZGRkZGRiIsInQiOjF9/TWVtb3J5/hugh-is-life-personal-use-italic.png" alt="Logo Memory"></a>
            <a href="current_revision.php">Révisions en cours</a>
            <a href="decouvrir.php">Découvrir</a>
        </nav>
        <i class="fa-regular fa-circle-user" id="profil"></i>
        <div id="arrowProfil" class="arrow-up notclicked"></div>
        <div id="funcProfil" class="funcprofil notclicked">
            <a href="update_data_page.php">Modifier mes données</a>
            <a href="mycreations.php">Gérer mes créations</a>
            <a href="my_revision.php">Mes révisions</a>
            <a href="deconnexion.php">Déconnexion</a>
        </div>
    </header>
    <main>
        <h1 class="titre titre--creations"><?php print_r($categorie[0]['nom']) ?> <a href="CRUD/theme/create.php">Ajouter un thème</a></h1>
        <section class="categorie">
            <?php
            if (count($themes) > 0) {
                for ($i = 0; $i < count($themes); $i++) {
                    if ($themes[$i]['id_categorie'] == $_GET['id_categorie']) {
                        if ($themes[$i]['public'] || $themes[$i]['id_user'] == $_SESSION['id']) {
            ?>
                            <article>
                                <a href="theme.php?id_theme=<?php echo $themes[$i]['id'] ?>"><?php echo $themes[$i]['nom'] ?></a>
                                <br>
                            </article>
            <?php
                        }
                    }
                }
            } else {
                echo "Il n'y a pas encore de thèmes publiés dans cette catégorie";
            }
            ?>
        </section>
    </main>
</body>

</html>